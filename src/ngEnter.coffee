angular
  .module 'ngEnter', []
  .directive 'ngEnter', ->
    {
      restrict : 'A'
      link : ( scope, $elem, attr, ctrls ) ->
        $elem.bind 'keypress', ( e ) ->
          if e.which is 13
            scope.$apply ->
              scope.$eval attr.ngEnter
    }

